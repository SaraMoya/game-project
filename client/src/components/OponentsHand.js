import React  from 'react'
import { URL } from '../config.js'

const OponentsHand = (props) => {
    console.log('oponentssHand =>', props.oponentssHand)
    return (props.oponentsHand.map((ele, i) => <div key={i} className='hand'>
        <a  className='oponentCard' onClick={() => props.pickCard(i, 'oponentsHand')}>
                                        <img src={`${URL}/assets/cardBack.png`}></img>
                                    </a>
        </div>)
    )
} 

export default OponentsHand

