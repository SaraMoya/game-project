import React , { useState } from 'react'
import axios from 'axios' 
import { URL } from '../config.js'

const ProductsComponent = (props) => {
    const [setError] = useState({})

    const removeProduct = (id) => {
		let url = `${URL}/products/remove`
		axios.post(url, {product_id: id}) 
		.then((res)=>{
			props.getProducts()
		}).catch((error)=>{
		  setError({error:'something went wrong'})
		})
      }
      
    return (
    props.products.map(ele => <div className='product'>
        <img alt={ele.product} src={`${URL}/assets/${ele.image.file}`}/>
        <h4>{ele.product}</h4>
        <p>{ele.description}</p>
        <p>Category: {ele.category}</p>
        <p>type: {ele.type}</p>
        {/* {ele.type === 'character' ? (<p>Physical resistance: {ele.physicalResistance}</p>, <p>Magical resistance: {ele.magicalResistance}</p>) 
        : (<p>Physical damage: {ele.physicalDamage}</p>, <p>Magical damage: {ele.magicalDamage}</p>)} */}
         {ele.type === 'character' ? <p>Physical resistance: {ele.physicalResistance}</p> : <p>Physical damage: {ele.physicalDamage}</p>}
         {ele.type === 'character' ? <p>Magical resistance: {ele.magicalResistance}</p> : <p>Magical damage: {ele.magicalDamage}</p>}
        <h5>{ele.price}</h5>
        <div>
            <button onClick={(e) => props.handleClick(ele) }>edit</button>
            <button onClick={(e) => removeProduct(ele._id) }>delete</button>
        </div>
    </div>)
    )
} 

export default ProductsComponent