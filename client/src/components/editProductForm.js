import React , { useState } from 'react'
import axios from 'axios' 
import { URL } from '../config.js'

const EditProductForm = (props) => {

    const [ field , setField ] = useState({
        key: '',
        val: '',
        category: props.product.category
    })

    const [ form , setValues ] = useState({
		product    : props.product.product,
        description : props.product.description,
        category_id: props.product.category_id,
        price: props.product.price,
        category: props.product.category,
        type: props.product.type,
        physicalDamage: props.product.physicalDamage, 
        magicalDamage: props.product.magicalDamage, 
        physicalResistance: props.product.physicalResistance, 
        magicalResistance: props.product.magicalResistance
	})
    
    const [image, setImage] = useState({
        file: null,
        image: props.product.image
      })
    
    const [ uploadSuccess , setUploadSuccess ] = useState(false)
    const [message] = useState('Image upload!')

	const handleChange = e => {
        let index = e.nativeEvent.target.selectedIndex, text;
        return !index
        ? (setValues({...form,[e.target.name]:e.target.value}),
        setField({...field, key:[e.target.name],val:e.target.value}))
        : ( text = e.nativeEvent.target[index].text,
            setValues({...form,[e.target.name]:e.target.value, category:text}),
            setField({...field, key:[e.target.name],val:e.target.value, category: text})
        )
    }

    const handleFileInputChange = e => {setUploadSuccess(false)
         setImage({...image,file:e.target.files[0]})}
    const getBase64 = (file, cb) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        cb(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
    }
    const handleSubmitFile = async () => {
        let idCardBase64 = '';
        getBase64(image.file, (result) => {
            idCardBase64 = result;
            console.log('idCardBase64 ==>', idCardBase64)
            setImage({...image, image:result})
        });
        setUploadSuccess(true)
        setField({...field, key: 'image', val: image.image})
    }

    const editProduct = e => {
        e.preventDefault()
        let url = `${URL}/products/update`
		axios.post(url, {
            _id: props.product._id,
            newProduct: form.product,
            newPrice: form.price,
            newDescription: form.description,
            newCategory_id: form.category_id,
            newCategory: form.category,
            newType: form.type,
            newImage: image.image,
            newPhysicalDamage: form.product.physicalDamage, 
            newMagicalDamage: form.product.magicalDamage, 
            newPhysicalResistance: form.product.physicalResistance, 
            newMagicalResistance: form.product.magicalResistance
        }) 
		.then((res)=>{
            props.getProducts()
            setValues({...form, product: props.product.product, description: props.product.description, category_id: props.product.category_id, price: props.product.price, category: props.product.category, type: props.product.type, image: props.product.image,  physicalDamage: props.product.physicalDamage, magicalDamage: props.product.magicalDamage, physicalResistance: props.product.physicalResistance, magicalResistance: props.product.magicalResistance})
			
		}).catch((error)=>{
			console.log('something went wrong', error)
		})	
    }

    const editProductField = e => {
        e.preventDefault()
        let url = `${URL}/products/updateField`
		axios.post(url, {
            _id: props.product._id,
            key: field.key,
            val: field.val,
            category: field.category
        }) 
		.then((res)=>{
            props.getProducts()
			
		}).catch((error)=>{
			console.log('something went wrong', error)
		})	
    }

    return (
        <div className='form_container'>
            <label>New image</label>
            <div>
                <input type="file" onChange={handleFileInputChange}/>
                {uploadSuccess ? <p>{message}</p> : null}
                <button onClick={()=>handleSubmitFile()}>submit file</button>
                <button onClick={editProductField}>edit</button>
            </div>
            <form onSubmit={editProductField} onChange={handleChange}>
                <label>New product name</label>
                <div>
                    <input name='product' value={form.product}></input>
                    <button>edit</button>
                </div>
            
                <label>New description</label>
                <div>
                    <input name='description' value={form.description}></input>
                    <button>edit</button>
                </div>

                <div>
                    <select value={form.type} name='type'  >
                        <option>Select new type</option>
                        <option value='weapon'>weapon</option>
                        <option value='character'>character</option>
                    </select>
                    <button>edit</button>
                </div>

                <div>
                    <select value={form.category_id} name='category_id'  >
                        <option>Select new category</option>
                        {props.categories.map((ele,i) => (
                            <option key={i} value={ele._id}>{ele.category}</option>
                            ))}
                    </select>
                    <button>edit</button>
                </div>

                <div>
                    <label>Physical damage</label>
                    <input name='physicalDamage' value={form.physicalDamage}></input>
                    <button>edit</button>
                </div>
               
                <div>
                    <label>Magical damage</label>
                    <input name='magicalDamage' value={form.magicalDamage}></input>
                    <button>edit</button>
                </div>
                
                <div>
                    <label>Physical resistance</label>
                    <input name='physicalResistance' value={form.physicalResistance}></input>
                    <button>edit</button>
                </div>
               
                <div>
                    <label>Magical resistance</label>
                    <input name='magicalResistance' value={form.magicalResistance}></input> 
                    <button>edit</button>
                </div>
               
                <label>New price</label>
                <div>
                    <input name='price' value={form.price}></input>
                    <button>edit</button>
                </div>
            </form>
            <button onClick={editProduct}>Edit product</button>
            <button onClick={props.handleClick}>Cancel</button>
        </div>
    )
} 

export default EditProductForm

