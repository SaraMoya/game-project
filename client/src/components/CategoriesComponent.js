import React from 'react';

const CategoriesComponent = (props) => {
    return (
    props.categories.map(ele => <h3 contentEditable="true"
    onBlur={e => props.editCategory(e,ele._id)}>{ele.category}</h3>)
    )
} 

export default CategoriesComponent

