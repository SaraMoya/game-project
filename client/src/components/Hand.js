import React from 'react'
import { URL } from '../config.js'

const Hand = (props) => {
    return props.playersHand.map((ele, i) =>
                <div key={i}>
                    <a onClick={() => props.pickCard(i)}>
                        <img className='weaponEquipped' src={`${URL}/assets/${ele.weaponImage}`}></img>
                        <img src={`${URL}/assets/${ele.characterImage}`}></img>
                    </a>
                </div>)
}

export default Hand

