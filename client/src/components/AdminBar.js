import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = (props) => (
   <div className='navbar'>
     <div className='navbarElements'>
   	  <NavLink
        exact
        to={"/products"}
	  >
        Products
      </NavLink>

      <NavLink
        exact
        to={"/Categories"}
      >
        Categories
      </NavLink>

      <button className='logout' onClick={()=>{props.logout()}}>Log out</button>
      </div>
   </div>
)
   
export default Navbar

