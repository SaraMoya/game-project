import React, { useState } from 'react'
import axios from 'axios'
import { URL } from '../config.js'
import { Progress } from 'reactstrap';

const AddProductForm = (props) => {
    const [form, setValues] = useState({
        product: '',
        description: '',
        category_id: '',
        price: '',
        category: '',
        physicalDamage: '',
        magicalDamage: '',
        physicalResistance: '',
        magicalResistance: ''
    })

    const [selectedFile, setSelectedFile] = useState([])

    const [loaded, setLoaded] = useState(0)

    const [uploadSuccess, setUploadSuccess] = useState(false)
    const [message] = useState('Image upload!')

    const handleChange = e => {
        let index = e.nativeEvent.target.selectedIndex, text;
        return !index
            ? setValues({ ...form, [e.target.name]: e.target.value })
            : (text = e.nativeEvent.target[index].text,
                setValues({ ...form, [e.target.name]: e.target.value, category: text })
            )
    }

    // const handleFileInputChange = e => {setUploadSuccess(false)
    //     setImage({...image,file:e.target.files[0]})}
    // const getBase64 = (file, cb) => {
    // let reader = new FileReader();
    // reader.readAsDataURL(file);
    // reader.onload = function () {
    //     cb(reader.result)
    // };
    // reader.onerror = function (error) {
    //     console.log('Error: ', error);
    // };

    // }
    // const handleSubmitFile = async () => {
    //     let idCardBase64 = '';
    //     getBase64(image.file, (result) => {
    //         idCardBase64 = result;
    //         setImage({...image, image:result})
    //     });
    //     setUploadSuccess(true)     
    // }


    // putting image to state onChange
    const onChangeHandler = event => {
        var files = event.target.files
        if (maxSelectFile(event) && checkMimeType(event) && checkFileSize(event)) {
            // if return true allow to setState
            setSelectedFile([...files])
            setLoaded(0)

        }
    }

    // checking that the file is an image
    const checkMimeType = (event) => {
        //getting file object
        let files = event.target.files
        //define message container
        let err = []
        // list allow mime type
        const types = ['image/png', 'image/jpeg', 'image/gif']
        // loop access array
        for (var x = 0; x < files.length; x++) {
            // compare file type find doesn't matach
            if (types.every(type => files[x].type !== type)) {
                // create error message and assign to container   
                err[x] = files[x].type + ' is not a supported format\n';
            }
        };
        for (var z = 0; z < err.length; z++) {// if message not same old that mean has error 
            // discard selected file
            event.target.value = null
        }
        return true;
    }

    // checking that only one file is selected
    const maxSelectFile = (event) => {
        let files = event.target.files
        if (files.length > 1) {
            const msg = 'Only 1 image can be uploaded at a time'
            event.target.value = null
            console.log(msg)
            return false;
        }
        return true;
    }

    // checking the filesize
    const checkFileSize = (event) => {
        let files = event.target.files
        let size = 5000000
        // 500 kbs
        let err = [];
        for (var x = 0; x < files.length; x++) {
            if (files[x].size > size) {
                err[x] = files[x].type + 'is too large, please pick a smaller file\n';
            }
        };
        for (var z = 0; z < err.length; z++) {// if message not same old that mean has error 
            // discard selected file
            console.log(err[z])
            event.target.value = null
        }
        return true;
    }

    const addProduct = e => {
        e.preventDefault()
        const data = new FormData()
        for (var x = 0; x < selectedFile.length; x++) {
            data.append('file', selectedFile[x])
            data.append('product', form.product)
            data.append('price', form.price)
            data.append('description', form.description)
            data.append('category_id', form.category_id)
            data.append('category', form.category)
            data.append('type', form.type)
            data.append('physicalDamage', form.physicalDamage)
            data.append('magicalDamage', form.magicalDamage)
            data.append('physicalResistance', form.physicalResistance)
            data.append('magicalResistance', form.magicalResistance)
    }

    let url = `${URL}/products/add`
    axios.post(url, data)
        .then((res) => {
            props.getProducts()
            setValues({ ...form, product: '', description: '', category_id: '', price: '', category: '', type: '', physicalDamage: '', magicalDamage: '', physicalResistance: '', magicalResistance: '' })
            //setImage({...image, image: null, file: null})

        }).catch((error) => {
            console.log(error, 'something went wrong')
        })
}

return (
    <div className='form_container'>
        <label>Image</label>
        {/* <input type="file" onChange={handleFileInputChange}/> */}
        {/* =============================================================================== */}
        <div className="offset-md-3 col-md-6">
            <div className="form-group files">
                <label>Upload Your File </label>
                <input type="file" className="form-control" multiple onChange={onChangeHandler} />
            </div>
            <div className="form-group">

                <Progress max="100" color="success" value={loaded} >{Math.round(loaded, 2)}%</Progress>

            </div>

            {selectedFile != null ? <button type="button" className="btn btn-success btn-block" >Upload</button> : null}

        </div>
        {/* =============================================================================== */}
        {/* {uploadSuccess ? <p>{message}</p> : null}
            <button onClick={()=>handleSubmitFile()}>submit file</button> */}
        <form onSubmit={addProduct} onChange={handleChange}>
            <label>Product name</label>
            <input name='product' value={form.product}></input>
            <label>Description</label>
            <input name='description' value={form.description}></input>
            <select value={form.type} name='type'  >
                <option>Select type</option>
                <option value='weapon'>weapon</option>
                <option value='character'>character</option>
            </select>
            <select value={form.category_id} name='category_id'  >
                <option>Select category</option>
                {props.categories.map((ele, i) => (
                    <option key={i} value={ele._id}>{ele.category}</option>
                ))}
            </select>
            <label>Price</label>
            <input name='price' value={form.price}></input>
            <label>Physical damage</label>
            <input name='physicalDamage' value={form.physicalDamage}></input>
            <label>Magical damage</label>
            <input name='magicalDamage' value={form.magicalDamage}></input>
            <label>Physical resistance</label>
            <input name='physicalResistance' value={form.physicalResistance}></input>
            <label>Magical resistance</label>
            <input name='magicalResistance' value={form.magicalResistance}></input>
            <button>Add product</button>
        </form>
    </div>
)
}

export default AddProductForm

