import React from 'react'
import Checkout from '../containers/Checkout.js'
import { StripeProvider, Elements } from 'react-stripe-elements'

const Stripe = (props) => {
    return  <StripeProvider apiKey={'pk_test_BWoemDK1UZw5HgjSlYI9dLUb0024GiGTXz'}>
              <Elements>
                <Checkout {...props}/>
              </Elements>
            </StripeProvider>
}

export default Stripe