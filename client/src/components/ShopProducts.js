import React, { useState } from 'react'
import { URL } from '../config.js'

const ShopProducts = (props) => {

    return (
        props.filteredProducts.map(ele => {
            let clicked = false
            return <div key={ele._id} className='product'>
                <div className='addToCart'>
                    <img alt={ele.product} src={`${URL}/assets/${ele.image.file}`} />
                     <button onClick={() => {
                        props.addToCart(ele)
                        clicked = true
                    }}>Add to cart</button> 
                </div>
                <div>
                    <h4>{ele.product}</h4>
                    <p>{ele.description}</p>
                    {ele.type === 'character' ? <p>Physical resistance: {ele.physicalResistance}</p> : <p>Physical damage: {ele.physicalDamage}</p>}
                    {ele.type === 'character' ? <p>Magical resistance: {ele.magicalResistance}</p> : <p>Magical damage: {ele.magicalDamage}</p>}
                    <h5>Price: {ele.price}</h5>
                </div>
            </div >
        })
    )
}

export default ShopProducts