import React  from 'react'
import { URL } from '../config.js'
// import { Redirect } from 'react-router-dom'

const Cart = (props) => {
    return ( <div  className='cart'>
    {props.cartProducts.map((ele, i) => <div key={ele._id} className='cartProduct'>
            <div>
                <img alt={ele.product} src={`${URL}/assets/${ele.image.file}`}/>
                <p>{ele.product}</p>
            </div>
            <div>
                <button onClick={() => props.modifyQuantity('decrease', i)}>-</button>
                <p>{ele.quantity}</p>
                <button onClick={() => props.modifyQuantity('increase', i)}>+</button>
            </div>
            <h5>Price: {ele.price}</h5>
    </div>)}
    {props.cartProducts.length > 0 ? <div>
        <h3>Total: {props.total}</h3>
        <button onClick={() => props.history.push('/checkout')}>Buy</button>
    </div> : <p>Your cart is empty, add some products</p>}
    </div>
    )
} 

export default Cart