import React from 'react'
import { URL } from '../config.js'

const ProfileItems = (props) => {
    return (props.items.length > 1 ? props.items.map((ele, i) => <div key={i} className='product'>
        <div className='addToCart'>
            <img alt={ele.product} src={`${URL}/assets/${ele.image.file}`} />
            <button>Add to deck</button>
        </div>
        <div>
            <h4>{ele.product}</h4>
            <p>{ele.description}</p>
            {ele.type === 'character' ? <p>Physical resistance: {ele.physicalResistance}</p> : <p>Physical damage: {ele.physicalDamage}</p>}
            {ele.type === 'character' ? <p>Magical resistance: {ele.magicalResistance}</p> : <p>Magical damage: {ele.magicalDamage}</p>}
            {/* {ele.type === 'character'? <button>Equip a weapon</button> : <button>Equip to a character</button> } */}
        </div>
    </div>) : null
    )

    //hello
}

export default ProfileItems