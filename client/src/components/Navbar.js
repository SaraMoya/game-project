import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = (props) => (
   <div className='navbar'>
     <div className='navbarElements'>
     <NavLink
        exact
        to={"/play"}
      >
        Play 
      </NavLink> 
      
     <NavLink
        exact
        to={"/shop"}
      >
        Shop
      </NavLink> 

      <NavLink
        exact
        to={"/profile"}
      >
        Profile
      </NavLink> 

      

      <button className='logout' onClick={()=>{props.logout()}}>Log out</button>
      </div>
   </div>
)
   
export default Navbar


