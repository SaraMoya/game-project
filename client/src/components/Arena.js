import React from 'react'
import { URL } from '../config.js'

const Arena = (props) => {
    return (<div className='arena'>
        <div className='oponentsArena'>

            {props.oponentsArena.map((ele, i) => <div key={i}>
                <img src={`${URL}/assets/${ele.characterImage}`}></img>
            </div>)}
        </div>
        <div className='playersArena'>

            {props.playersArena.map((ele, i) => <div key={i}>
                <img src={`${URL}/assets/${ele.characterImage}`}></img>
            </div>)}
        </div>
    </div>)
}

export default Arena

