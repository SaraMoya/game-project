
const calculateTotal = (fun) => {
    let items = JSON.parse(localStorage.getItem('cart')) || []
    let total = 0
    items.map(ele => total += (ele.price * ele.quantity))
    return (fun(total))
}

export { calculateTotal }