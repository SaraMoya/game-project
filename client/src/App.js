import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import Login from './containers/Login.js'
import Register from './containers/Register.js'
import Shop from './containers/Shop.js'
import Navbar from './components/Navbar.js'
import { URL } from './config.js'
import AdminBar from './components/AdminBar.js'
import Categories from './containers/Categories.js'
import Products from './containers/Products.js'
import Stripe from './components/stripe.js'
import Profile from './containers/Profile.js'
import Match from './containers/Match.js'
import PaymentSuccess from './containers/payment_success.js'
import PaymentError from './containers/payment_error.js'
import UsersConnected from './containers/usersConnected.js'

import { socket } from './socket.js'


function App() {
   const [isLoggedIn, setIsLoggedIn] = useState(false)
   const [isAdmin, setIsAdmin] = useState(false)
   const [products, setProducts] = useState([])
   const [categories, setCategories] = useState([])
   const [userItems, setUserItems] = useState([])
   const [error, setError] = useState({})
   const [cart] = useState(JSON.parse(localStorage.getItem('cart')) || [])
   const [userEmail, setUserEmail] = useState('')
   const [userId, setUserId] = useState()
   const [onMatch, setOnMatch] = useState(false)
   const [coin, setUserCoin] = useState()

   useEffect(() => {
      return () => {
         socket.emit('disconnect_player', token)
         socket.disconnect()
      }
   }, [])



   const token = JSON.parse(localStorage.getItem('token'))
   const verify_token = async () => {
      if (token === null) return setIsLoggedIn(false)
      try {
         axios.defaults.headers.common['Authorization'] = token
         const response = await axios.post(`${URL}/users/verify_token`)
         setUserEmail(response.data.email)
         setUserId(response.data.userId)
         if (response.data.admin === true) { setIsAdmin(true) }
         return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false)
      }
      catch (error) {
         console.log(error)
      }
   }
   useEffect(() => {
      verify_token()
   }, [verify_token])


   const login = (token, id) => {
      localStorage.setItem('token', JSON.stringify(token))
      setUserId(id)
      localStorage.setItem('userId', JSON.stringify(id))
      setIsLoggedIn(true)
   }
   const logout = () => {
      localStorage.clear();
      setIsLoggedIn(false)
      setIsAdmin(false)
      socket.emit('disconnect_player', token)
      socket.disconnect()
      return <Redirect to={'/'} />
   }

   const getProducts = () => {
      axios.get(`${URL}/products/`, {
         headers: {
            'Access-Control-Allow-Origin': '*',
         },
         proxy: {
            host: 'http://localhost',
            port: 3000
         }
      })
         .then((res) => {
            setProducts([...res.data])
         })
         .catch((error) => {
            setError({ error, message: 'something went wrong' })
         })
   }

   const getCategories = () => {

      axios.get(`${URL}/categories/`, {
         headers: {
            'Access-Control-Allow-Origin': '*',
         },
         proxy: {
            host: 'http://localhost',
            port: 3000
         }
      })
         .then((res) => {
            setCategories([...res.data])
         })
         .catch((error) => {
            console.log(error)
         })
   }

   const getUserItems = () => {
      axios.post(`${URL}/users/find_items`, { email: userEmail })
         .then((res) => {
            setUserItems([...res.data])
         })
         .catch((error) => {
            console.log(error)
         })
   }

   const getUserCoin = () => {
      axios.post(`${URL}/users/find_coin`, { email: userEmail })
         .then((res) => {
            setUserCoin([...res.data])
         })
         .catch((error) => {
            console.log(error)
         })
   }


   useEffect(() => {
      getProducts()
      getCategories()
      // getUserCoin()
   }, [])

   useEffect(() => {
      if (userEmail !== '') { getUserItems() }
   }, [userEmail])

  


   return (
      <Router>
         {isAdmin ? <AdminBar logout={logout} /> : !onMatch ? <Navbar logout={logout} /> : null}
         <Route exact path='/' render={props => {
            return isLoggedIn
               ? <Redirect to={'/shop'} />
               : isAdmin ? <Redirect to={'/products'} />
                  : <Login login={login} {...props} />
         }} />
         <Route path='/register' component={Register} />
         <Route path='/shop' render={props => {
            return !isLoggedIn
               ? <Redirect to={'/'} />
               : <Shop categories={categories} {...props} products={products} />
         }} />
         <Route path='/categories' render={props => {
            return !isAdmin
               ? <Redirect to={'/'} />
               : <Categories getCategories={getCategories} categories={categories} />
         }} />
         <Route path='/products' render={props => {
            return !isAdmin
               ? <Redirect to={'/'} />
               : <Products categories={categories} products={products} error={error} getProducts={getProducts} />
         }} />
         <Route exact path='/checkout' render={props => {
            return !isLoggedIn
               ? <Redirect to={'/'} />
               : <Stripe cart={cart} {...props} />
         }} />

         {/* <Route exact path='/checkout'                render={props => <Checkout cart={cart}/>}/> */}
         <Route path='/profile' render={props => {
            return !isLoggedIn
               ? <Redirect to={'/'} />
               : <Profile products={products} userItemsIds={userItems} coin={coin} {...props} />
         }} />
         <Route path='/match' render={props => {
            return !isLoggedIn
               ? <Redirect to={'/'} />
               : <Match  {...props} products={products} setOnMatch={setOnMatch} />
         }} />
         <Route path='/payment/success' render={props => {
            return !true
               ? <Redirect to={'/'} />
               : <PaymentSuccess  {...props} userEmail={userEmail} />
         }} />
         <Route path='/payment/error' render={props => {
            return !true
               ? <Redirect to={'/'} />
               : <PaymentError  {...props} />
         }} />
         <Route path='/play' render={props => {
            return !isLoggedIn
               ? <Redirect to={'/'} />
               : <UsersConnected  {...props} />
         }} />
      </Router>
   );
}

export default App;
