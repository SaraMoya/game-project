import { URL } from './config.js'

import socketIOClient from "socket.io-client";

const socket = socketIOClient(`${URL}`, {transports: ['websocket']});
export { socket }
