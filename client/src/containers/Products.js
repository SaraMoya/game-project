import React from 'react'
import ProductsComponent from '../components/ProductsComponent.js'
import AddProductForm from '../components/addProductForm.js'
import EditProductForm from '../components/editProductForm.js'


class Products extends React.Component {
	state ={
		editClicked: false,
		product: {}
	}

	handleClick = (item) => {
		let { editClicked } = {...this.state}
		editClicked = !editClicked
		this.setState({ editClicked })

		let { product } = {...this.state}
		product = item
		this.setState({ product })

	}
	// getProducts = () => {
	// 	axios.get(`${URL}/products/`)
	// 	.then((res)=>{
	// 	  this.setState({ products: res.data})
	// 	})
	// 	.catch((error)=>{
	// 	  this.setState({error:'something went wrong'})
	// 	})
	//   }
	
	//   componentDidMount(){this.getProducts()}

	  render() {
		return (
			<div className='products'> 
				<div>	
					{this.state.editClicked === false ? <AddProductForm categories={this.props.categories} getProducts={this.props.getProducts}/> : <EditProductForm product={this.state.product} handleClick={this.handleClick} categories={this.props.categories} getProducts={this.props.getProducts}/>}
				</div>
				{this.props.products.length > 0 ? <ProductsComponent handleClick={this.handleClick} getProducts={this.props.getProducts} products={this.props.products}/> : null}	
			</div> 
		);
	  }
}

export default Products










