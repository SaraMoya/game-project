import React, {useState, useEffect} from 'react';
//import Product from '../components/product'
import axios from 'axios'
import {URL} from '../config.js'
import { injectStripe } from "react-stripe-elements";
import {calculateTotal} from '../helpers/calculateTotal.js'

const Checkout = (props) => {
  const [total, setTotal] = useState()
  const products = JSON.parse(localStorage.getItem('cart'))

  useEffect( () => {
    calculateTotal(setTotal)
   },[])
 
    
const createCheckoutSession = async() => {
  try{
    axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('token'))
    const response = await axios.post(`${URL}/payment/create-checkout-session`,{products})
    console.log(response.data.email)
    return response.data.ok 
    ? ( 
       localStorage.setItem('ids', JSON.stringify(response.data.ids)),
       localStorage.setItem('sessionId', JSON.stringify(response.data.sessionId)),
       redirect(response.data.sessionId)
      )
    : props.history.push('/payment/error')
    }catch(error){
      props.history.push('/payment/error')
    }
}

const redirect = (sessionId) => {
    props.stripe.redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      }).then(function (result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
} 

   return <div className='checkout_container'>
            
            <h3>Checkout - Check products before pay</h3> 
            <div  className='checkoutProduct'>
                {products.map( (ele,i) => <div key={i} >
                            <img alt={ele.product} src={`${URL}/assets/${ele.image.file}`}/>
                            <div>
                              <p>{ele.product}</p>
                              <h5>Price: {ele.price}</h5>
                            </div>
                    </div>)
                }
            </div>    
              <div className='total'>Total : {total} €</div>
              <button onClick={() => props.history.push('/shop')}>Go back</button>
              <button className='button' onClick={()=>createCheckoutSession()}>PAY</button>
          </div>
}

export default injectStripe(Checkout)

