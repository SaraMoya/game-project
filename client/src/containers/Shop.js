import React , { useState , useEffect } from 'react'
import ShopProducts from '../components/ShopProducts.js'
import Cart from '../components/Cart.js'
import {calculateTotal} from '../helpers/calculateTotal.js'


const Shop = (props) => {
	console.log('shop props', props.products)
	const tempCart = JSON.parse(localStorage.getItem('cart')) || []
	const [filteredProducts, setFilteredProducts] =useState([...props.products])
	const [productName, setSearch] =useState('')
	const [categoryFilter , setFilters ] = useState('')
	const [onSearch, setOnSearch] = useState(false)
	const [cartProducts, setCartProducts] = useState([])
	const [showCart, setShowCart] = useState(false)
	const [total, setTotal] = useState(0)
	const [clicked, setClicked] = useState(false)
	

	useEffect( () => {
	 setFilteredProducts([...props.products])
	},[props])

	useEffect( () => {
	  calculateTotal(setTotal)
	  tempCart === null 
	  ? localStorage.setItem('cart', JSON.stringify([]))
	  : setCartProducts([...tempCart])
	},[])

	
	const handleInputChange = e => {
		setSearch(e.target.value)
	}

	const searchByName = e => {
		e.preventDefault()
		let tempProducts = props.products.filter(ele => ele.product.match(productName))
		
		setFilteredProducts([...tempProducts])
		setOnSearch(true)
	}

	const goBack = () => {setOnSearch(false)
		setFilteredProducts([...props.products])}

	const handleChange = e => {
		setFilters(e.target.value)
		let tempProducts = props.products.filter(ele => ele.category_id === e.target.value)
		e.target.value === 'all' || !e.target.value ? 
		setFilteredProducts([...props.products])
		: setFilteredProducts([...tempProducts])
	}
	
	const showCartClicked = () => {
		setShowCart(!showCart)
	}
	
	const addToCart = item => {
		item['quantity'] = 1
		let alreadyInCart = tempCart.some(ele => ele._id === item._id)
		if (!alreadyInCart) {tempCart.push(item)
			setCartProducts([...tempCart])}
		

		localStorage.setItem('cart', JSON.stringify(tempCart));
		calculateTotal(setTotal)
		setClicked(true)
		setTimeout(() => { setClicked(false); }, 1500);
	}

	const modifyQuantity = (action, i) => {
		if (action === 'decrease') {tempCart[i].quantity -= 1 
			calculateTotal(setTotal)
		} else {tempCart[i].quantity += 1 
				calculateTotal(setTotal)
		}

		if (tempCart[i].quantity === 0){tempCart.splice(i, 1)}

		setCartProducts([...tempCart])
		localStorage.setItem('cart', JSON.stringify(tempCart));
	}


		return (
			<div className='shop'>
				<nav>
					<div className='filters'>
						<form onSubmit={searchByName}>
							<input name='productName' value={productName} onChange={handleInputChange} placeholder='Search by name'></input>
							<i className="fas fa-search"></i>
						</form>
						<form>
							<select className='select' value={categoryFilter} onChange={handleChange}  >
							<option value='all'>All products</option>
							{props.categories.map((ele,i) => (
								<option key={i} value={ele._id}>{ele.category}</option>
								))}
							</select>
						</form>
					</div>
					<div className='cartIcon'>
						<button onClick={showCartClicked}><i className="fas fa-shopping-cart"></i>Cart</button>
						{clicked ? <h4>Added!</h4> : null}
					</div>
				</nav>
				{showCart ? <Cart history={props.history} cartProducts={cartProducts} modifyQuantity={modifyQuantity} total={total}/> : null}
				{onSearch ? <button onClick={goBack}>Go back to all products</button> : null}
				<div className='products'>
					<ShopProducts addToCart={addToCart} {...props} filteredProducts={filteredProducts} />
				</div>
			</div> 
		)
}


export default Shop










