import React, { useState, useEffect } from 'react'
import ProfileItems from '../components/ProfileItems.js'



const Profile = (props) => {
	console.log(props)
	const [items, setItems] = useState([])

	useEffect(() => {
		getFullItems()
	}, [])

	const getFullItems = () => {
		let temp = []
		props.userItemsIds.map(id => {
			let item = props.products.filter(product => product._id === id)
			temp.push(...item)
		})
		setItems(temp)
	}


	return (
		<div className='profile'>
			{/* <div>
				<p>{props.coin}coins</p>
			</div> */}
			<h3>Your items: </h3>
			<div className='products'>
				<ProfileItems items={items} />
			</div>
		</div>
	)
}

export default Profile



