import React, { useEffect, useState } from 'react'
import { socket } from '../socket.js'
import { URL } from '../config.js'

const UsersConnected = (props) => {
    const user2 = JSON.parse(localStorage.getItem('user2'))
    const token = JSON.parse(localStorage.getItem('token'))
    const [isInMatch, setIsInMatch] = useState(false)
  
    useEffect(() => {
        setIsInMatch(false)
        socket.emit('get_connected_users', token)
        socket.on('found_users', (connectedUsers) => {
            setUsers([...connectedUsers])
        })
        socket.on('room_create_message', (user2, message) => {
            localStorage.setItem('user2', JSON.stringify(user2))
            setMessage({ ...message, text: `${message}`, display: 'flex' })
        })
    }, [])

    useEffect(() => {
        return () => {
            setIsInMatch(true)
        }
    }, [])

    const [users, setUsers] = useState([])
    const [message, setMessage] = useState({
        text: '',
        display: 'none'
    })



    const handleCreateRoom = (oponentId) => {
        localStorage.setItem('user2', JSON.stringify(oponentId))
        socket.emit('create_room_intent', token, oponentId)
        //=======  receive response from the other user =======
        socket.on('create_room_intent_response', (value) => {
            value
                ? props.history.push(`/match`)
                : alert('Sorry, the other player refuse your invitation')
        })
    }

    const handleCreateRoomResponse = (value) => {
        return value
            ? (
                socket.emit('response_to_create_room', token, true, user2),
                props.history.push(`/match`)
            ) : (
                socket.emit('response_to_create_room', token, false, user2),
                setMessage({ ...message, text: ``, display: 'none' }),
                localStorage.removeItem('user2')
            )
    }

    return (<div className='play'>
        <div>
            <h1>Welcome to the game!</h1>
        
            <img alt='characters preview' src={`${URL}/assets/characters.png`}/>
        </div>
        <div>
            <div style={{ display: message.display }}
                className='message_container'>
                {message.text}
                <button onClick={() => handleCreateRoomResponse(true)}>Play</button>
                <button onClick={() => handleCreateRoomResponse(false)}>Cancel</button>
            </div>
            <div className='usersList'>
                <h3>Choose a player to enter a match:</h3>
                {users.map((ele, i) => <div key={i} className='user'>
                    <p>{ele._id}</p>
                    {isInMatch ? <p>In match</p> : <button onClick={() => handleCreateRoom(ele._id)}>Play</button>}
                </div>)
                }
            </div>

        </div>
    </div>
    )
}

export default UsersConnected


