import React , { useState } from 'react'
import Axios from 'axios' 
import { URL } from '../config.js'

const Login = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
          const response = await Axios.post(`${URL}/users/login`,{
          	email:form.email,
          	password:form.password
          })
          setMessage(response.data.message)
          if( response.data.ok ){
              setTimeout( ()=> {
				  props.login(response.data.token, response.data.userId)
				  props.history.push('/shop')
			  },1500)    
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}
	return <div className='form_container'>
				<form onSubmit={handleSubmit}
					onChange={handleChange}>
					<label>Email</label>    
					<input name="email"/>
					<label>Password</label>
					<input name="password" type='password'/>
					<button>log in</button>
					<div className='message'><h4>{message}</h4></div>
				</form>
				<a href='/register'>Register</a>
		   </div>
}

export default Login










