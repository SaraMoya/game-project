import React , { useState , useEffect } from 'react'
import { URL } from '../config.js'
import Hand from '../components/Hand.js'
import OponentsHand from '../components/OponentsHand.js'
import Arena from '../components/Arena.js'

const Play = (props) => {
    const [cards] = useState([])
    const [playersDeck] = useState([])
    const [oponentsDeck] = useState([])
    const [playersHand, setPlayersHand] = useState([])
    const [oponentsHand, setOponentsHand] = useState([])
    const [playersArena] = useState([])
    const [oponentsArena] = useState([])
    const [playerCard, setPlayerCard] = useState({})
    const [oponentCard, setOponentCard] = useState({})
    const [winner, setWinner] = useState('')

    useEffect( () => {
        compare() 
    }, [oponentCard])

    useEffect (() => {
        composeCards(props.products)
    }, [props.products])

    useEffect( () => {
        buildDecks() 
    }, [cards])

    useEffect( () => {
        endGame() 
    }, [winner])

    const composeCards = (products) => { 
        let characterImage, physicalResistance, magicalResistance, weaponImage, physicalDamage, magicalDamage
        let weapons = products.filter( pro => pro.type === 'weapon').sort( () => Math.random() - .5)
        let characters = products.filter( pro => pro.type === 'character').sort( () => Math.random() - .5)
       
        characters.map((ele, i) =>  { 
            characterImage = ele.image.file
            physicalResistance = ele.physicalResistance
            magicalResistance = ele.magicalResistance
            weaponImage = weapons[i].image.file 
            physicalDamage = weapons[i].physicalDamage 
            magicalDamage = weapons[i].magicalDamage  

            //tempCards
            cards.push({characterImage, physicalResistance, magicalResistance, weaponImage, physicalDamage, magicalDamage})
        })
    }

    const buildDecks = () => {
        // let tempCards = cards
        // playersDeck.push(tempCards.splice(0, 5))
        // oponentsDeck.push(tempCards)
        playersDeck.push(...cards.sort( () => Math.random() - .5))
        oponentsDeck.push(...cards.sort( () => Math.random() - .5))
    }
  
    const pickHand = player => {
        let count 
        let tempHand 
        return player === 'player' ? 
            ( count = playersHand.length,
            tempHand = playersHand,
            playersDeck.map((ele, i) => count < 5 ? (tempHand.push(ele), playersDeck.splice(i, 1), count ++) : null),
            setPlayersHand([...tempHand]),
            pickHand('oponent'))
        : player === 'oponent' ? ( count = oponentsHand.length,
            tempHand = oponentsHand,
            oponentsDeck.map((ele, i) => count < 5 ? (tempHand.push(ele), oponentsDeck.splice(i, 1), count ++) : null),
            setOponentsHand([...tempHand])
            )  : null
            
    }

    const pickCard = (indx, hand) => {
        return hand === 'playersHand' ? (setPlayerCard(playersHand[indx]), playersHand.splice(indx, 1), pickCard(indx, 'oponentsHand'))
        : (setOponentCard(oponentsHand[indx]) , oponentsHand.splice(indx, 1))
    }

    const compare = () => {
        if (oponentCard.magicalDamage > playerCard.magicalResistance) {
            oponentsArena.push(oponentCard)
            setOponentCard({})
        }  else if (oponentCard.magicalDamage < playerCard.magicalResistance) {
            playersArena.push(playerCard)
            setPlayerCard({})
        } else if (oponentCard.magicalDamage == playerCard.magicalResistance) {
            if (oponentCard.physicalDamage > playerCard.physicalResistance) {
                oponentsArena.push(oponentCard)
                setOponentCard({})
            }
            if (oponentCard.physicalDamage < playerCard.physicalResistance ) {
                playersArena.push(playerCard)
                setPlayerCard({})
            }
        }
        return tellWinner()
    }
    

    const tellWinner = () => {
    if (playersArena.length === 5 && oponentsArena.length < 5) {
        setWinner('player')
    } else if (oponentsArena.length === 5 && playersArena.length < 5) {
        setWinner('opponent')
    } else if  (playersArena.length > 0 
                && playersDeck.length === 0
                && oponentsDeck.length === 0
                && playersHand.length === 0
                && oponentsHand.length === 0) {
                    	playersArena.length > oponentsArena.length ? setWinner('player') : setWinner('opponent')
    }}

    const endGame = () => {
       return  winner ? window.prompt(`${winner} won!`) : null
    }

		return <div className='play'>
                <div className='oponentSpace'>
                {/* {oponentsDeck.length > 0 ?  <a onClick={() => pickHand('player')}><img src={`${URL}/assets/playerDeck.png`}></img></a> : <img src={`${URL}/assets/outofcards.png`}></img>} */}
                <a><img src={`${URL}/assets/playerDeck.png`}></img></a>
                    <OponentsHand oponentsHand={oponentsHand} pickCard={pickCard}/>
                    <p>Oponent's avatar</p>
                </div>
                <div className='arena'><Arena playersArena={playersArena} oponentsArena={oponentsArena} /></div>
                <div className='playerSpace'>
                    <p>player's avatar</p>
                    <Hand playersHand={playersHand} pickCard={pickCard}/>
                    {/* {playersDeck.length > 0 ?  <a onClick={() => pickHand('player')}><img src={`${URL}/assets/playerDeck.png`}></img></a> : <img src={`${URL}/assets/outofcards.png`}></img>} */}
                    <a onClick={() => pickHand('player')}><img src={`${URL}/assets/playerDeck.png`}></img></a>
                </div>
			</div> 
}
//cart: add modifyQuantity and total
export default Play















