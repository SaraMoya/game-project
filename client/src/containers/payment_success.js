import React, { useEffect } from 'react'
import axios from 'axios'
import {URL} from '../config.js'
  
const PaymentSuccess = (props) => {
    useEffect( () => {
      //getSessionData()
      createOrder()
    },[props.userEmail])
   //  const getSessionData = async () => {
   //     try{
   //       const sessionId = JSON.parse(localStorage.getItem('sessionId'))
   //       const response = await axios.get(`${URL}/payment/checkout-session?sessionId=${sessionId}`)
   //       localStorage.removeItem('sessionId')
   //       console.log('== response ==>',response)
   //       //if you need the products list in this page, you can find them in : response.data.session.display_items
   //     }catch(error){
   //       //handle the error here, in case of network error
   //     }
   //  }
   const createOrder = async () => {

      try{
         if(props.userEmail !== ''){
            const response = await axios.post(`${URL}/orders/create`,{
                                                         orderCart: JSON.parse(localStorage.getItem('ids')),
                                                         userEmail: props.userEmail
                                                         //userName: ???????
            })
            localStorage.removeItem('cart')
            localStorage.removeItem('sessionId')
            localStorage.removeItem('ids')
      }
      }catch(error){

      }
   }
    return <div className='payment_message_container'>
              <div style={{border:'2px solid  #35BFDE'}} className='message_box'>
                 <div className='message_box_left'>
                    <img alt='smile_icon' className='faceIcon' src={'https://res.cloudinary.com/estefanodi2009/image/upload/v1578495645/images/smile.png'}/>
                 </div>
                 <div style={{color:'#35BFDE'}} className='message_box_right'>
                    Payment Successfull
                 </div>
              </div>
               
           </div>
           
}

export default PaymentSuccess