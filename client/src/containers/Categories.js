import React from 'react'
import axios from 'axios';
import CategoriesComponent from '../components/CategoriesComponent.js'
import { URL } from '../config.js'


class Categories extends React.Component {
	state ={
		error:'',
		data: ''
	}
  
     
	  handleChange = e =>{
		let { data } = {...this.state}
		data = e.target.value
		this.setState({ data })
	  }
    
	  addCategory = () => {
		let url = `${URL}/categories/add`
		axios.post(url, {category: this.state.data}) 
		.then((res)=>{
			this.props.getCategories()
			let { data } = {...this.state}
			data = ''
			this.setState({ data })
	  
		}).catch((error)=>{
		  this.setState({error:'something went wrong'})
		})
	  }
	
	  removeCategory = () => {
		let url = `${URL}/categories/remove`
		axios.post(url, {category: this.state.data}) 
		.then((res)=>{
			this.props.getCategories()
			let { data } = {...this.state}
			data = ''
			this.setState({ data })
				
		}).catch((error)=>{
		  this.setState({error:'something went wrong'})
		})
	  }

	  
	  editCategory = (e,_id) => {
		let url = `${URL}/categories/update`
		axios.put(url, { _id, newCategory:  e.currentTarget.textContent}) 
		.then((res)=>{
			this.props.getCategories()		
		}).catch((error)=>{
		  this.setState({error:'something went wrong'})
		}) 
	  }

	  render() {
		return (
			<div className='categories'>
                <div>
                    <h2>Current categories:</h2>
                    {this.props.categories.length > 0 ? <CategoriesComponent categories={this.props.categories} editCategory={this.editCategory} handleOnFocus={this.handleOnFocus}/> : null}
                </div>
                <div>
				  <input value={this.state.data} onChange={this.handleChange}/>
				  <button onClick={this.addCategory}>Add category</button> 
				  <button onClick={this.removeCategory}>Remove category</button> 
				</div>
			</div> 
		);
	  }
}

export default Categories










