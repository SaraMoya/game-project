import React, { useState, useEffect } from 'react'
import { URL } from '../config.js'
import Hand from '../components/Hand.js'
import Arena from '../components/Arena.js'
import { socket } from '../socket.js'

const Match = (props) => {
    const [cards] = useState([])
    const [playersDeck] = useState([])
    const [oponentsDeck] = useState([])
    const [playersHand, setPlayersHand] = useState([])
    const [oponentsHand, setOponentsHand] = useState([{},{},{},{},{}])
    const [playersArena, setPlayersArena] = useState([])
    const [oponentsArena, setOponentsArena] = useState([])
    const [playerCard, setPlayerCard] = useState()
    const [oponentCard, setOponentCard] = useState()
    const [winner, setWinner] = useState('')

    const token = JSON.parse(localStorage.getItem('token'))
    const userId = JSON.parse(localStorage.getItem('userId'))
    const user2 = JSON.parse(localStorage.getItem('user2'))

    useEffect(() => {
        composeCards(props.products)
    }, [props.products])

    useEffect(() => {
        buildDecks()
    }, [cards])

    useEffect(() => {
        endGame()
    }, [winner])

    useEffect(() => {
        socket.on('get_player_card', (playerCard) => {
            setOponentCard(playerCard)
        })
    }, [playerCard])

    useEffect(() => {
        socket.on('get_winner_card', (obj) => {
            let tempPlayersArena = playersArena
            let tempOponentsArena = oponentsArena
            obj.winner === true ? tempPlayersArena.push(obj.userCard) : tempOponentsArena.push(obj.userCard)
            setPlayersArena(tempPlayersArena)
            setOponentsArena(tempOponentsArena)
            console.log('players arena -->', playersArena)
            console.log('oponents arena -->', oponentsArena)
        })
        props.setOnMatch(true)
        socket.on('player_left_the_match', (obj) => {
            debugger
            alert(`${obj.message}`)
            debugger
            props.setOnMatch(false)
            props.history.push(`/play`)
        })
    }, [])

    useEffect(() => {
        return () => {
            socket.emit('leave_the_match', token, user2)
            props.setOnMatch(false)
            props.history.push(`/play`)
        }
    }, [])

    const composeCards = (products) => {
        let characterImage, physicalResistance, magicalResistance, weaponImage, physicalDamage, magicalDamage
        let weapons = products.filter(pro => pro.type === 'weapon').sort(() => Math.random() - .5)
        let characters = products.filter(pro => pro.type === 'character').sort(() => Math.random() - .5)

        characters.map((ele, i) => {
            characterImage = ele.image.file
            physicalResistance = ele.physicalResistance
            magicalResistance = ele.magicalResistance
            weaponImage = weapons[i].Eimage.file
            physicalDamage = weapons[i].physicalDamage
            magicalDamage = weapons[i].magicalDamage

            cards.push({ characterImage, physicalResistance, magicalResistance, weaponImage, physicalDamage, magicalDamage })
        })
    }

    const buildDecks = () => {
        playersDeck.push(...cards.sort(() => Math.random() - .5))
    }

    const pickHand = player => {
        let count
        let tempHand
        return player === 'player' ?
            (count = playersHand.length,
                tempHand = playersHand,
                playersDeck.map((ele, i) => count < 5 ? (tempHand.push(ele), playersDeck.splice(i, 1), count++) : null),
                setPlayersHand([...tempHand]))
            : player === 'oponent' ? (count = oponentsHand.length,
                tempHand = oponentsHand,
                oponentsDeck.map((ele, i) => count < 5 ? (tempHand.push(ele), oponentsDeck.splice(i, 1), count++) : null),
                setOponentsHand([...tempHand])
            ) : null

    }

    const pickCard = indx => {
        socket.emit('send_player_card', userId, playersHand[indx], user2)
        setPlayerCard(playersHand[indx])
        playersHand.splice(indx, 1)
        return tellWinner()
    }


    const tellWinner = () => {
        if (playersArena.length === 5 && oponentsArena.length < 5) {
            setWinner('player')
        } else if (oponentsArena.length === 5 && playersArena.length < 5) {
            setWinner('opponent')
        } else if (playersArena.length > 0
            && playersDeck.length === 0
            && oponentsDeck.length === 0
            && playersHand.length === 0
            && oponentsHand.length === 0) {
            playersArena.length > oponentsArena.length ? setWinner('player') : setWinner('opponent')
        }
    }

    const endGame = () => {
        return winner ? window.prompt(`${winner} won!`) : null
    }

    return <div className='match'>
        <div className='oponentSpace'>
            <div>
            <img  src={`${URL}/assets/forest_deck.png`}></img>
            </div>
            <div>
            {oponentCard ? <img className='oponentCard' src={`${URL}/assets/${oponentCard.characterImage}`}></img> : null}  
            <oponentsHand oponentsHand={oponentsHand}/>
            </div>
            <div>
            <img src={`${URL}/assets/cernunnos.png`}></img>
            </div>
           
        </div>
      
        <div className='arena'><Arena playersArena={playersArena} oponentsArena={oponentsArena} /></div>
        <div className='playerSpace'>
            <div>
            <img src={`${URL}/assets/hel.png`}></img>
            </div>
            <div className='hand'>
            {playerCard ? <img className='playerCard' src={`${URL}/assets/${playerCard.characterImage}`}></img> : null}
            <Hand playersHand={playersHand} pickCard={pickCard} />
            </div>
            <div>
            <a onClick={() => pickHand('player')}><img src={`${URL}/assets/moon_deck.png`}></img></a>
            </div>
        </div>
    </div>
}

export default Match







