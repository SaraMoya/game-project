const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/cardController');


// router.get('/', controller.findCategories)

router.post('/add', controller.addCard)
router.post('/remove', controller.removeCard)

module.exports = router;

