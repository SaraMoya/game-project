const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/orderController');


router.post("/create", controller.createOrder);


module.exports = router