const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/categoriesController');


router.get('/', controller.findCategories)

router.post('/add', controller.addCategory)
router.put('/update', controller.updateCategory)
router.post('/remove', controller.removeCategory)

module.exports = router;


//=====>   /categories/add      POST            // add new category to DB
//=====>   /categories/remove   POST            // remove category from DB
//=====>   /categories/update   POST            // update category
//=====>   /categories          GET             // GET all categories