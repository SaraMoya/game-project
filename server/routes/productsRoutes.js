const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/productsController');

const multer = require('multer')

const storage = multer.diskStorage({
    destination: function (req, image, cb) {
        cb(null, 'public')
    },
    filename: function (req, image, cb) {
        cb(null, Date.now() + '-' +image.originalname )
    }
    })
    
const upload = multer({ storage: storage }).single('file')


router.get('/:category', controller.findProductsInCategory)
router.get('/', controller.findAllProducts)

router.post('/product', controller.findProduct)
router.post('/add', upload, async (req,res) => {
    return await controller.addProduct(req,res,upload)
})
router.post('/update', controller.updateProduct)
router.post('/updateField', controller.updateProductField)
router.post('/remove', controller.removeProduct)

module.exports = router;

