const io = require("../index");
const {
  getConnectedUsers,
  createRoomIntent,
  responseToCreateRoom,
  leaveTheMatch,
  play,
  sendPlayerCard,
  disconnectPlayer
} = require("../controllers/socketController");
io.on("connection", socket => {
    socket.on("get_connected_users", getConnectedUsers.bind(null, io, socket));
    socket.on("create_room_intent", createRoomIntent.bind(null, io, socket));
    socket.on(
      "response_to_create_room",
      responseToCreateRoom.bind(null, io, socket)
    );
    socket.on("play", play.bind(null, io, socket));
    socket.on("send_player_card", sendPlayerCard.bind(null, io, socket));
    socket.on("disconnect_player", disconnectPlayer.bind(null, io, socket));
    socket.on("leave_the_match", leaveTheMatch.bind(null, io, socket))
  });