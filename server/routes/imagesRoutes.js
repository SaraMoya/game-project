const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/imagesController');

router.get('/upload', controller.uploadImage)

module.exports= router

