const nodemailer = require('nodemailer')

const transport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: process.env.NODEMAILER_EMAIL,
        pass: process.env.NODEMAILER_PASSWORD,
    }
});

const sendOrderEmail = (res,userEmail) => {
    const emails = [process.env.NODEMAILER_EMAIL, userEmail]
    const subjects = ['admin subject', 'user subject']
    const body = ['admin body', 'user body']
    try {

    emails.map(async (ele, i) => {
        const mailOptions = {
            to: ele,
            subject: subjects[i],
            html: '<p>' + subjects[i] + '</p><p><pre>' + body[i] + '</pre></p>'
        }
            const response = await transport.sendMail(mailOptions)
        })
        
        return res.json({ on: true, message: 'emails sent' })
    }
    catch (err) {
        console.log('ERROR', err)
        return res.json({ ok: false, message: err })
    }
}


module.exports = sendOrderEmail 