const express  = require('express');
const path = require('path')
const app = express();
const port = process.env.port || 4242;
const http = require("http");
require('dotenv').config();
const server = http.Server(app);
const feather = require('feather-icons')

//================ CORS ================================
const cors = require('cors');
app.use(cors());

// app.use(cors({
//     origin: 'http://localhost:3030'
//   }));

//================ SOCKET ==============================
const socket = require("socket.io");
const io = socket(server);
module.exports = io;
// =============== BODY PARSER SETTINGS =====================
const bodyParser= require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// =============== DATABASE CONNECTION =====================
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
async function connecting(){
    try {
        await mongoose.connect(process.env.MLAB_URL)
        console.log('Connected to the DB')
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting()    
// =============== ROUTES ==============================

app.use('/users', require('./routes/users.routes'));
app.use('/products', require('./routes/productsRoutes'));
app.use('/categories', require('./routes/categoriesRoutes'));
app.use('/payment', require('./routes/paymentRoutes.js'))
app.use('/cards', require('./routes/cardsRoutes.js'))
app.use('/orders', require('./routes/orderRoutes.js'))
require("./routes/socketRoutes")

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));
app.use('/assets', express.static(path.join(__dirname, './public')))

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

// =============== START SERVER =====================
server.listen(port, () => 
console.log(`server listening on port ${port}`
));
