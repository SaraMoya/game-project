// == CARD SCHEMA ==

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CardSchema = new Schema({
    characterImage: {
        type: String,
        required: true,
    },
    weaponImage: {
        type: String,
        required: true,
    },
    physicalDamage : {
        type: Number,
        required: true,
    },
    magicalDamage : {
        type: Number,
        required: true,
    },
    physicalResistance : {
        type: Number,
        required: true,
    },
    magicalResistance : {
        type: Number,
        required: true,
    }
})
const cardModel = mongoose.model('cards', CardSchema);
module.exports =  cardModel;



