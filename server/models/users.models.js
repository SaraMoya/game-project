const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email:{ type:String, unique:true, required:true },
    password:{ type:String, required:true },
    admin: {type:Boolean, required: true},
    items: {type: Array},
    coin: {type: Number}
});
module.exports = mongoose.model('users', userSchema);

//email required: true, unique:true type:string
//password required: true, unique:true, type: string

//username required false unique: true type: string 
//admin:false 
//profileImage required false unique false type?
//usercards required false unique false 
//userCharacters required false unique false
//userWeapons required false unique false
//userCoins required false unique false
//userWinsTrack required false unique false



//ADMIN USER
//email required true, unique true type string
//password required true, unique true type string