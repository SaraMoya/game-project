// == PRODUCTS SCHEMA ==

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ProductsSchema = new Schema({
    category_id:{
        type:Schema.Types.ObjectId, 
        required:true,
        ref:'Category'
    },
    product: {
        type: String,
        required: true,
        unique: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    }, 
    category: {
        type: String,
        required: false,
        ref: 'Category'
    },
    type: {
        type: String,
        required: true,
    },
    image: {
        type: Object,
        required: true,
        default: {
            pathname: 'public/image-placeholder.png',
            file: 'default image'
        }
    },
    physicalDamage : {
        type: Number,
        required: true,
    },
    magicalDamage : {
        type: Number,
        required: true,
    },
    physicalResistance : {
        type: Number,
        required: true,
    },
    magicalResistance : {
        type: Number,
        required: true,
    },
    Eimage: {
        pathname: String,
        file: String,
    }
})
const productsModel = mongoose.model('products', ProductsSchema);
module.exports =  productsModel;


// // INSERT PRODUCTS
// productsModel.create({product:{name: 'Hel'}, category_id:'some _id taken from a language schema goes here...'})
// // FIND ALL WORDS OF A SPECIFIC LANGUAGE:
// productsModel.find({category_id: 'some language id from the language schema goes here'})

