// == CATEGORIES SCHEMA ==
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CategoriesSchema = new Schema({
    category:{
        type:String, required:true, unique:true
    }
})
module.exports =  mongoose.model('categories', CategoriesSchema);
