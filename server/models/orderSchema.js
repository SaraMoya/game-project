// == ORDER SCHEMA ==
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const OrderSchema = new Schema({
    userEmail : {
        type: String,
        required: true
    }, 
    userName : {
        type: String, 
        default: ''
    }, 
    orderCart : {
        type: Array,
        required: true
    },
    orderDate : {
        type: Date,
        default: Date.now
    }
})
module.exports =  mongoose.model('orders', OrderSchema);
