const Products = require('../models/cardSchema');

class CardController {
    //GET FIND ALL PRODUCTS
     async findAllCards(req, res){
        try{
            const cards = await Cards.find({});
            res.send(cards);
        }
        catch(e){
            res.send({error:e,message:'Something went wrong'})
        }
    }

    // //POST FIND CARD
    async findCard(req, res){
        let { card_id } = req.body
        try{
            const card = await Cards.find({_id:card_id});
            res.send(card);
        }
        catch(e){
            res.send({error:e,message:"Couldn't find the card"})
        }
    }

    //POST ADD CARD
    async addCard (req, res) {
        let { characterImage, weaponImage, physicalDamage, magicalDamage, magicalResistance, physicalResistance} = req.body;
        
        try{
            const done = await Cards.create({characterImage, weaponImage, physicalDamage, magicalDamage, magicalResistance, physicalResistance}); 
            res.send(done)
        }
        catch(e){
            res.send('ERROR: card NOT added',e)
        }
    }

    
    //POST REMOVE CARD
    async removeCard (req, res){
        let { card_id } = req.body;
        try{
            const removed = await Cards.deleteOne({ _id:card_id });
            res.send({removed});
        }
        catch(error){
            res.send("ERROR: Couldn't remove card");
        };
    }

};

module.exports = new CardController();

