const Order = require('../models/orderSchema');
const User       = require('../models/users.models'); 
const sendOrderEmail = require('../helpers/sendOrderEmail.js')

class OrderController  {

    async createOrder(req, res) {
        const {userEmail, orderCart} = req.body
        try{
            const order = await Order.create({userEmail, orderCart})
            const user = await User.updateOne({ email: userEmail },{$push:{items:[...orderCart]}})
            sendOrderEmail(res, userEmail)
        }catch(error){
            console.log('ORDER CONTROLLER ERROR', error)
        }
    }
}

module.exports = new OrderController();
