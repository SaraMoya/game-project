const Users = require("../models/users.models");
const User = require('../models/users.models'); 
const jwt = require("jsonwebtoken");
const jwt_secret = process.env.JWT_SECRET;
const connectedUsers = [];
const rooms = []

const verify_token = async token => {
  const decoded = await jwt.verify(token, jwt_secret);
  return decoded;
};

const getConnectedUsers = async (io, socket, token) => {
  try {
    //==========  check if the token is valid
    const decoded = await verify_token(token);
    //==========  check if I'm already in the connectedUsers array    
    const index = connectedUsers.findIndex(user => user.userId === decoded._id);
    index === -1
      ? connectedUsers.push({ userId: decoded._id, socketId: socket.id })
      : (connectedUsers[index].socketId = socket.id);
      //========== get connected users ids
      const ids = [];
      connectedUsers.forEach(user => {
        // user.userId !== decoded._id ? ids.push(user.userId) : null;
        ids.push(user.userId)
      });
      //========== get connected users from the database
      const users = await Users.find({ _id: { $in: ids } });
    const index2 = users.findIndex(user => user._id == decoded._id);
    users.splice(index2, 1)
    //========== send all connected users to the client
    socket.emit("found_users", users);
    // io.emit("found_users", users);
  } catch (error) {
    console.log("error ===>", error);
  }
};

const createRoomIntent = async (io, _, token, user2) => {
  try {
    const decoded = await verify_token(token);
    // do something here if the token is not valid
    const user2idx = connectedUsers.findIndex(user => user.userId === user2);
    io.to(`${connectedUsers[user2idx].socketId}`).emit(
      "room_create_message",
      decoded._id,
      `${decoded.email} wants to play with you`
    );
  } catch (error) {
    console.log("error ===>", error);
  }
};

const responseToCreateRoom = async (io, _, token, value, user1) => {
  try {
    const decoded = await verify_token(token);
    const user1idx = connectedUsers.findIndex(user => user.userId === user1);
    if (value) {
      rooms.push(
        {
          user1: { _id: user1, arena: [] },
          user2: { _id: decoded._id, arena: [] },
          count: 0
        }
      )
    }
    io.to(`${connectedUsers[user1idx].socketId}`).emit(
      "create_room_intent_response",
      value
    );
  } catch (error) {
    console.log("error ===>", error);
  }
};

const disconnectPlayer = async (io, _, token) => {
  try {
    const decoded = await verify_token(token);
    const index = connectedUsers.findIndex(user => user.userId === decoded._id);
    connectedUsers.splice(index, 1)
    const ids = [];
    connectedUsers.forEach(user => {
      ids.push(user.userId)
    });
    //========== get connected users from the database
    const users = await Users.find({ _id: { $in: ids } });
    io.emit("found_users", users);
  } catch (error) {
    console.log('error', error)
  }
}

const leaveTheMatch = async (io, _, token, user2) => {
  try {
    const decoded = await verify_token(token);
    let roomIndex = rooms.findIndex(room =>
      room.user1._id == user2 || room.user1._id == decoded._id
      && room.user2._id == user2 || room.user2._id == decoded._id)

    if (roomIndex !== -1) {
      const index = connectedUsers.findIndex(user => user.userId == user2);

      await User.updateOne({ _id: decoded._id }, { $inc: { coin: -5 } })
      await User.updateOne({ _id: user2 }, { $inc: { coin: 50 } })
  
      rooms.splice(roomIndex, 1)
      io.to(`${connectedUsers[index].socketId}`).emit('player_left_the_match',
        { message: 'The opponent left the match, you are the winner now: +50 coins'})
    }
  } catch (error) {
    console.log('error', error)
  }
}

const play = async (io, _, token, i, value, user2) => {
  try {
    const decoded = await verify_token(token);
    // do something here if the token is not valid
    const index = connectedUsers.findIndex(user => user.userId === user2);
    io.to(`${connectedUsers[index].socketId}`).emit("other_user_move", {
      i,
      value
    });
  } catch (error) {
    console.log("error ===>", error);
  }
};



const sendPlayerCard = (io, socket, userId, playerCard, user2) => {
  console.log('rooms ==>',rooms)

  const idx = rooms.findIndex(room => userId === room.user1._id || userId === room.user2._id && user2 === room.user1._id || user2 === room.user2._id)
  // handle -1 index
  userId === rooms[idx].user1._id ? rooms[idx].user1.card = playerCard : rooms[idx].user2.card = playerCard
  const index = connectedUsers.findIndex(user => user.userId === user2);
  const count = rooms[idx].count
  io.to(`${connectedUsers[index].socketId}`).emit('get_player_card', playerCard, connectedUsers, count)

  rooms[idx].count++
  rooms[idx].count % 2 === 0 ? compare(idx, io) : null
}

const compare = (idx, io) => {
  let user1Card = rooms[idx].user1.card
  let user2Card = rooms[idx].user2.card
  let winner
  //user 1 player, user 2 oponent
  if (user2Card.magicalDamage > user1Card.magicalResistance) {
    rooms[idx].user2.arena.push(user2Card)
    winner = 'user2'
  } else if (user2Card.magicalDamage < user1Card.magicalResistance) {
    rooms[idx].user1.arena.push(user1Card)
    winner = 'user1'
  } else if (user2Card.magicalDamage == user1Card.magicalResistance) {
    if (user2Card.physicalDamage > user1Card.physicalResistance) {
      rooms[idx].user2.arena.push(user2Card)
      winner = 'user2'
    }
    if (user2Card.physicalDamage < user1Card.physicalResistance) {
      rooms[idx].user1.arena.push(user1Card)
      winner = 'user1'
    }
  }
  const user1 = rooms[idx].user1._id
  const user2 = rooms[idx].user2._id
  const index = connectedUsers.findIndex(user => user.userId === user1);
  const index2 = connectedUsers.findIndex(user => user.userId === user2);
  io
    .to(`${connectedUsers[winner === 'user1' ? index : index2].socketId}`)
    .emit('get_winner_card', { userCard: winner === 'user1' ? user1Card : user2Card, winner: winner === 'user1' ? true : false })
  io
    .to(`${connectedUsers[winner === 'user2' ? index : index2].socketId}`)
    .emit('get_winner_card', { userCard: winner === 'user2' ? user2Card : user1Card, winner: winner === 'user2' ? true : false })

  rooms[idx].user1.card = ''
  rooms[idx].user2.card = ''

}

module.exports = {
  getConnectedUsers,
  createRoomIntent,
  responseToCreateRoom,
  play,
  sendPlayerCard,
  disconnectPlayer,
  leaveTheMatch
};