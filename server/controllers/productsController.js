const Products = require('../models/productsSchema');
const multer = require('multer')

class ProductsController {
    //GET FIND ALL PRODUCTS
    async findAllProducts(req, res) {
        try {
            const products = await Products.find({});
           
            res.send(products);
        }
        catch (e) {
            res.send({ error: e, message: 'Something went wrong' })
        }
    }

    //GET FIND ALL PRODUCTS IN CATEGORY
    async findProductsInCategory(req, res) {
        let { category } = req.params

        try {
            const products = await Products.find({ category_id: category });
            res.send(products);
        }
        catch (e) {
            res.send({ error: e, message: 'Something went wrong' })
        }
    }

    //POST FIND PRODUCT
    async findProduct(req, res) {
        let { product_id } = req.body
        try {
            const product = await Products.find({ _id: product_id });
            res.send(product);
        }
        catch (e) {
            res.send({ error: e, message: "Couldn't find the product" })
        }
    }

    //POST ADD PRODUCT
    async addProduct(req, res, upload) {
        let { product, price = Number(price), description, category_id, category, type, image, physicalDamage = Number(physicalDamage), magicalDamage = Number(magicalDamage), physicalResistance = Number(physicalResistance), magicalResistance = Number(magicalResistance) } = req.body;
        
        
        try {
            const createProduct = await Products.create({ 
                                                    product, 
                                                    price , 
                                                    description, 
                                                    category_id, 
                                                    category, 
                                                    type, 
                                                    image, 
                                                    physicalDamage , 
                                                    magicalDamage , 
                                                    physicalResistance , 
                                                    magicalResistance  
            })
         
            upload(req, res, async (err) => {
                if (err instanceof multer.MulterError) {
                    return res.status(500).json(err)
                    // A Multer error occurred when uploading.
                } else if (err) {
                    return res.status(500).json(err)
                    // An unknown error occurred when uploading.
                }
                // here we can add filename or path to the DB
                console.log(`this could go to the DB: ${req.file.filename}, or this: ${req.file.path}`)
                req.body._id = createProduct._id
                req.body.key = 'image'
                req.body.val = {
                    pathname: req.file.path,
                    file: req.file.filename
                }
                req.body.category = createProduct.category
             
                return await this.updateProductField(req, res)
                //return res.status(200).send(req.file)
                // Everything went fine.
            })
        }
        catch (e) {
            res.send('ERROR: product NOT added', e)
        }
    }

    //POST UPDATE PRODUCT 
    async updateProduct(req, res) {
        let { _id, newProduct, newPrice, newDescription, newCategory, newType, newImage, newPhysicalDamage, newMagicalDamage, newPhysicalResistance, newMagicalResistance } = req.body;
     
        try {
            const updated = await Products.updateOne(
                { _id }, { product: newProduct, price: newPrice, description: newDescription, category: newCategory, image: newImage, type: newType, physicalDamage: newPhysicalDamage, magicalDamage: newMagicalDamage, physicalResistance: newPhysicalResistance, magicalResistance: newMagicalResistance }
            );
            res.send({ updated });
        }
        catch (error) {
            res.send('ERROR: Product NOT updated');
        };
    }

    //to modify 1 by 1 or all, but not send the same data again
    async updateProductField(req, res) {
        let { _id, key, val, category } = req.body;
        try {
            const updated = await Products.updateOne(
                { _id }, { [key]: val, category }
            );
          
            res.send({ updated });
        }
        catch (error) {
            console.log('error', error)
            res.send('ERROR: Product NOT updated');
        };
    }

    //POST REMOVE PRODUCT
    async removeProduct(req, res) {
        let { product_id } = req.body;
        try {
            const removed = await Products.deleteOne({ _id: product_id });
            res.send({ removed });
        }
        catch (error) {
            res.send("ERROR: Couldn't remove product");
        };
    }

};

// productsModel.create({product:{name: 'Hel'}, category_id:'some _id taken from a language schema goes here...'})

module.exports = new ProductsController();

