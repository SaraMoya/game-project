const Categories = require('../models/categoriesSchema');

class CategoriesController {
    //GET FIND ALL
    async findCategories(req, res){
        try{
            const categories = await Categories.find({});
            res.send(categories);
        }
        catch(e){
            res.send({e})
        }
    }

     //POST ADD CATEGORY
     async addCategory (req, res) {
        let { category } = req.body;
        try{
            const done = await Categories.create({category});
            res.send(done)
        }
        catch(e){
            res.send('ERROR: category NOT added')
        }
    }

     // UPDATE CATEGORY
     async updateCategory (req, res){
        let { _id, newCategory } = req.body;
     
        try{
            const updated = await Categories.updateOne(
                { _id },{ category:newCategory }
             );
            res.send({updated});
        }
        catch(error){
            res.send('ERROR: Category NOT updated');
        };
    }

     // REMOVE CATEGORY
     async removeCategory (req, res){
        let { category } = req.body;
        try{
            const removed = await Categories.deleteOne({ category });
            res.send({removed});
        }
        catch(error){
            res.send("ERROR: Couldn't remove category");
        };
    }
};
module.exports = new CategoriesController();

