const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const jwt        = require('jsonwebtoken');
const jwt_secret = process.env.JWT_SECRET

const verify_token = async(req) => {
    const token = req.headers.authorization;
         const decoded = await jwt.verify(token, jwt_secret)
              return  !decoded ? false : decoded    
  }

const create_checkout_session = async (req,res) => {
    try{
        const decoded = await verify_token(req)
      
        if(!decoded)return res.send({ok: false, message:'User not recognised'})
        const domainURL = process.env.DOMAIN;
        const { products } = req.body;
        if(products.length < 1 || !products)return res.send({ok:false,message:'Please select at least 1 product'})
        let tempProducts = [], ids = []
        products.forEach( item => { 
            ids.push(item._id)
            tempProducts.push({
                name: item.product,
                amount: item.price * 100,
                images: [`http://localhost:4040/assets/item.image.file`],
                quantity: item.quantity,
                currency: process.env.CURRENCY
            })
            // item.currency = process.env.CURRENCY ; 
            // item.amount = item.price* 100;
            // delete item.price
        })
        // Create new Checkout Session for the order
        // Other optional params include:
        // [billing_address_collection] - to display billing address details on the page
        // [customer] - if you have an existing Stripe Customer ID
        // [payment_intent_data] - lets capture the payment later
        // [customer_email] - lets you prefill the email input in the form
        // For full details see https://stripe.com/docs/api/checkout/sessions/create
        session = await stripe.checkout.sessions.create({
        payment_method_types: process.env.PAYMENT_METHODS.split(", "),
        line_items: tempProducts,
        // ?session_id={CHECKOUT_SESSION_ID} means the redirect will have the session ID set as a query param
        success_url: `${domainURL}/payment/success?session_id={CHECKOUT_SESSION_ID}`,
        cancel_url: `${domainURL}/`
        });
        return res.send({ok:true,sessionId: session.id, ids: ids,email:decoded.email});
    }
    catch(error){
        console.log('ERROR =====>',error)
        return res.send({ok:false,message:error});
    }
}

const checkout_session = async (req,res) => {
    try{
        const { sessionId } = req.query;
        const session = await stripe.checkout.sessions.retrieve(sessionId);
        return res.send({ok:true,session:session});
    }catch(error){
        console.log('ERROR =====>',error.raw.message)
        return res.send({ok:false,message:error.raw.message});
    }
}

module.exports = {
    create_checkout_session,
    checkout_session
}
